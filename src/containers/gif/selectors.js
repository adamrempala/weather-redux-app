import { createSelector } from 'reselect';
import { prop } from 'ramda';

import { GIF_REDUCER_NAME } from './reducer';
import { NO_ERROR } from '../../weather-logic/const';

const getGifReducerState = prop(GIF_REDUCER_NAME);

export const currentGifSelector = createSelector(
    getGifReducerState,
    (state) => state.get('current')
)

export const isErrorGifSelector = createSelector(
    getGifReducerState,
    (state) => state.get('error') !== NO_ERROR ? true : false
)

export const loadingGifSelector = createSelector(
    getGifReducerState,
    (state) => state.get('loading')
)

export const timeSelector = createSelector(
    getGifReducerState,
    (state) => state.get('timestart')
)