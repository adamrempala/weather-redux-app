import React from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../themes';
import { GlobalStyle } from '../../global-styles';
import { useSelector } from 'react-redux'
import { modeSelector } from '../weather/selectors'
import { Weather } from '../weather'

export const App = () => {
    const mode = useSelector(modeSelector);
    return (
        <ThemeProvider theme={theme}>
            <>
                <GlobalStyle 
                    mode={mode}
                />
    
                <Weather />
            </>
        </ThemeProvider>
)};