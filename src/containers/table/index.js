import React from 'react';
import { useSelector } from 'react-redux';
import { RANGES, LOADING } from '../../weather-logic/const'
import { TableWrapper } from './components';
import { Row } from './components/row'
import { 
    modeSelector,
    rangeSelector,
    dailyWeatherSelector,
    hourlyWeatherSelector,
    loadingWeatherSelector,
    isErrorWeatherSelector
} from "../weather/selectors"
import { MainRow } from './components/mainrow';

export const Table = () => {
    const mode = useSelector(modeSelector);
    const range = useSelector(rangeSelector);
    const daily = useSelector(dailyWeatherSelector);
    const hourly = useSelector(hourlyWeatherSelector)
    const loading = useSelector(loadingWeatherSelector);
    const isError = useSelector(isErrorWeatherSelector);
    const prediction = range === RANGES.DAILY
     ? daily
     : hourly;

    if (loading !== LOADING.FALSE || isError === true) {
        return (
            <div></div>
        );
    }
    else {
        return (
            <TableWrapper
                mode={mode}
                range={range}
            >
                <MainRow range={range}/>
                {
                    prediction.map((predictionItem) => 
                    <Row
                        key={predictionItem.dt}
                        data={predictionItem}
                        range={range}
                    />)
                }
            </TableWrapper>
        )
    }
}