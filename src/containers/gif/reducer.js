import { fromJS } from 'immutable'
import {
    FETCH_GIFS_REQUEST,
    FETCH_GIFS_FAILURE,
    CLEAR_GIF,
    LOAD_GIF
} from './const'
import { LOADING, NO_ERROR } from '../../weather-logic/const'

export const GIF_REDUCER_NAME = 'Gif';

const initialState = fromJS({
    loading: LOADING.NOTHING,
    current: "", // Current gif URL
    timestart: null, // Time of request related to current GIF set
    error: NO_ERROR
})

export const gifReducer = (state=initialState, action) => {
    switch(action.type) {
        case CLEAR_GIF:
            return initialState;
        case FETCH_GIFS_REQUEST:
            return state
                .set('timestart', action.time)
                .set('loading', LOADING.TRUE)
                .set('error', NO_ERROR);
        case FETCH_GIFS_FAILURE:
            return state
                .set('loading', LOADING.FALSE)
                .set('error', action.payload);
        case LOAD_GIF:
            return state
                .set('loading', LOADING.FALSE)
                .set('current', action.data[action.iter % action.data.length])
                .set('error', NO_ERROR);
        default:
            return state;
    }
}