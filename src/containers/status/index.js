import React from 'react';
import { useSelector } from 'react-redux';
import {
    isWeatherNiceSelector,
    citySelector,
    rangeSelector,
    haveWeWeatherSelector,
    loadingWeatherSelector,
    isErrorWeatherSelector,
    longitudeSelector,
    latitudeSelector
} from '../weather/selectors';
import { StatusWrapper } from './components';
import { LOADING } from '../../weather-logic/const';
import Loader from 'react-loader-spinner'

export const Status = () => {
    const weatherNiceity = useSelector(isWeatherNiceSelector);
    let city = useSelector(citySelector);
    const lon = useSelector(longitudeSelector);
    const lat = useSelector(latitudeSelector);
    const haveWeather = useSelector(haveWeWeatherSelector);
    const loading = useSelector(loadingWeatherSelector);
    const isError = useSelector(isErrorWeatherSelector);
    
    const range = useSelector(rangeSelector);

    if (city === null) city = 'my location';

    let message = 'Please select a place';
    if (isError === true) {
        message = `Error. Please try again.`
    } else if (haveWeather === true)
        message = `Weather ${range} prediction for ${city} (${lon}, ${lat})\nThis week will be… ${weatherNiceity}!`;
    else if (loading === LOADING.NOTHING) {
        message = `Please choose location`
    } else if (loading === LOADING.TRUE) {
        message = `Loading…`
    }
    
    if (loading === LOADING.TRUE) {
        return(
            <Loader
                type="TailSpin"
                color="#00BFFF"
                height={50}
                width={400}
                timeout={3000} //3 secs

            />
        )
    } else {
        return (
            <StatusWrapper
                weatherNiceity={weatherNiceity}>
                {message}
            </StatusWrapper>
        );
    }
};
