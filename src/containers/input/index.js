import React from 'react';
import Autocomplete from 'react-autocomplete'
import { useDispatch } from 'react-redux'
import { setPredictionRequest } from '../weather/actions'
const options = require('./city.list.json')

export const Input = () => {
    
    const [value, setValue] = React.useState('');
    const [open, setOpen] = React.useState(false); // Used to close menu after select
    const dispatch = useDispatch();

    return(
        <Autocomplete
            items={options.slice(0, 500000)} // List of cities is too big, but first 500000 are satisfying.
            shouldItemRender={(item, value) => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1}
            renderItem={(item, isHighlighted) =>
                <div
                    key={item.id}
                    style={{ 
                        background: isHighlighted ? 'lightgray' : 'white',
                        height: '35px',
                        fontSize: '16px'
                    }}
                >
                {`${item.name}, ${item.country}`}
                </div>
            }
            getItemValue={(item) => item.name}
            value={value}
            open={open && value.length >= 3}
            onChange={e => {
                setValue(e.target.value)
                setOpen(true);
            }}
            onSelect={(value, item) => {
                setValue('');

                setOpen(false);
                dispatch(setPredictionRequest(
                    item.coord.lon, item.coord.lat, `${item.name}, ${item.country}`
                ));
            }}
        />
        // <div></div>
    )
}