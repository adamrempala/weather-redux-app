import { 
    WEATHERTYPES,
    MODES
} from '../weather-logic/const'

export const colors = {
    background: '#FFFFFF',
    text: '#000000',
    border: '#999999',

    location: '#9999FF',

    mode: {
        [MODES.LIGHT]: '#FFFFFF',
        [MODES.DARK]: '#333333',
        [MODES.UNKNOWN]: '#ffff00'
    },

    range: {
        background: '#999b66',
        color: '#FFFFFF'
    },

    button: {
        hoverBackground: '#dddddd'
    },

    weathertypes: {
        [WEATHERTYPES.UNKNOWN]: '#999999',
        [WEATHERTYPES.NOT_NICE]: '#FF0000',
        [WEATHERTYPES.PASSABLE]: '#FFFF00',
        [WEATHERTYPES.NICE]: '#00FF00'
    }
};
