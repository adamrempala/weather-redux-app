import React from 'react';
import { LocationWrapper } from './LocationWrapper';
import { useDispatch } from 'react-redux'
import { setPredictionRequest } from '../weather/actions'
import { setPredictionFailure } from '../weather/actions';
import { NO_CITY } from '../../weather-logic/const';

export const Location = () => {
        const dispatch = useDispatch();
        let lat;
        let lon;

        const onLocationClick = () => {
            navigator.geolocation.getCurrentPosition(location => {
                lat = location.coords.latitude;
                lon = location.coords.longitude;
                dispatch(setPredictionRequest(lon, lat, NO_CITY));
            },
            error => {
                dispatch(setPredictionFailure())
            });
        }

        return (
            <LocationWrapper onClick={() => onLocationClick()}>
                My location
            </LocationWrapper>
        )
};
