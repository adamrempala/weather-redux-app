import styled from 'styled-components';
import { theme } from 'styled-tools';

export const CellWrapper = styled.td`
    padding: ${theme('dims.basicSpacing')} ${theme('dims.bigSpacing')};
`;
