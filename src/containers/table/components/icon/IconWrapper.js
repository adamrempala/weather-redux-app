import styled from 'styled-components';

export const IconWrapper = styled.object`
    background: white;
    height: 70px;
    width: 70px;
    border-radius: 6px;
`