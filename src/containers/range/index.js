import { RangeWrapper } from './RangeWrapper'
import { RANGES } from '../../weather-logic/const'
import { rangeSelector } from '../weather/selectors'
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { changeRange } from '../weather/actions'

export const Range = () => {
    const range = useSelector(rangeSelector);
    const dispatch = useDispatch();
    const onRangeClick = () => dispatch(changeRange())
    const message = range === RANGES.DAILY ? 'Hourly' : 'Daily'

    return (
        <RangeWrapper
            onClick={ () => onRangeClick() }
        >
            {`${message} switch`}
        </RangeWrapper>
    )
}