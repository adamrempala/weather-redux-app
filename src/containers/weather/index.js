import React from 'react';
import { Dark } from '../dark'
import { Gif } from '../gif'
import { Location } from '../location'
import { Range } from '../range'
import { Status } from '../status'
import { Table } from '../table'
import { Input } from '../input'
import { useSelector } from 'react-redux';
import { modeSelector } from './selectors';
import { WeatherWrapper, SearchWrapper } from './components'

export const Weather = () => {
    const mode = useSelector(modeSelector);

    return (
        <WeatherWrapper
            mode={mode}
        >
            <SearchWrapper>
                <Input />
                <Location />
                <Range />
                <Dark />
            </SearchWrapper>
            <Gif />
            <Status />
            <table>
                <Table />
            </table>
        </WeatherWrapper>
    )
};