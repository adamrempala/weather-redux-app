import styled from 'styled-components';
import { theme } from 'styled-tools';

export const StatusWrapper = styled.p`
    background: ${({weatherNiceity}) =>
        theme(`colors.weathertypes.${weatherNiceity}`)};
    padding: 0 ${theme('dims.basicSpacing')} ${theme('dims.basicSpacing')};
    text-align: center;
    border-radius: 10px;
    width: 400px;
`;