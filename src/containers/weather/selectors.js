import { createSelector } from 'reselect';
import { prop } from 'ramda';

import { WEATHER_REDUCER_NAME } from './reducer';
import { WEATHERTYPES, NO_CITY, NO_ERROR, NO_WEATHER } from '../../weather-logic/const';
import {
    calculateNiceWeather,
    longitudeFormatter,
    latitudeFormatter,
    mostPopularWeatherType
} from '../../weather-logic/logic'

const getWeatherReducerState = prop(WEATHER_REDUCER_NAME);

export const displayModeSelector = createSelector(
    getWeatherReducerState,
    (weather) => weather.get('displayMode')
);

export const weatherDataSelector = createSelector(
    getWeatherReducerState,
    (weather) => weather.get('weatherData')
);

export const cacheSelector = createSelector(
    weatherDataSelector,
    (weather) => weather.get('cache')
);

export const rangeSelector = createSelector(
    displayModeSelector,
    (dmd) => dmd.get('range')
);

export const modeSelector = createSelector(
    displayModeSelector,
    (dmd) => dmd.get('mode')
);

export const citySelector = createSelector(
    weatherDataSelector,
    (weatherData) => weatherData.get('city')
);

export const isKnownCitySelector = createSelector(
    citySelector,
    (city) => city === NO_CITY ? false : true
);

export const forecastSelector = createSelector(
    weatherDataSelector,
    (weatherData) => weatherData.get('prediction')
);

export const weatherSelector = createSelector(
    forecastSelector,
    (prediction) => prediction.get('weather')
)

export const haveWeWeatherSelector = createSelector(
    weatherSelector,
    (weather) => weather === NO_WEATHER ? false : true
)

export const longitudeSelector = createSelector(
    weatherSelector,
    haveWeWeatherSelector,
    (weather, isWeather) => isWeather ? longitudeFormatter(weather.lon) : null
);

export const latitudeSelector = createSelector(
    weatherSelector,
    haveWeWeatherSelector,
    (weather, isWeather) => isWeather ? latitudeFormatter(weather.lat) : null
);

export const dailyWeatherSelector = createSelector(
    weatherSelector,
    haveWeWeatherSelector,
    (weather, isWeather) => isWeather ? weather.daily : null
)

export const hourlyWeatherSelector = createSelector(
    weatherSelector,
    haveWeWeatherSelector,
    (weather, isWeather) => isWeather ? weather.hourly : null
)

export const isWeatherNiceSelector = createSelector(
    dailyWeatherSelector,
    (dws) => dws === null ? WEATHERTYPES.UNKNOWN : calculateNiceWeather(dws)
)

export const mostPopularDailySelector = createSelector(
    dailyWeatherSelector,
    (dws) => mostPopularWeatherType(dws)
)

export const mostPopularHourlySelector = createSelector(
    hourlyWeatherSelector,
    (hws) => mostPopularWeatherType(hws)
)

export const isErrorWeatherSelector = createSelector(
    forecastSelector,
    (prediction) => prediction.get('error') !== NO_ERROR ? true : false
);

export const loadingWeatherSelector = createSelector(
    forecastSelector,
    (prediction) => prediction.get('loading')
);