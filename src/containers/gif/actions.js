import {
    FETCH_GIFS_REQUEST,
    FETCH_GIFS_FAILURE,
    CLEAR_GIF,
    LOAD_GIF,
} from './const'

export const fetchGifsRequest = (time, key) => ({
    type: FETCH_GIFS_REQUEST,
    time,
    key
})

export const loadGif = (data, iter, time) => ({
    type: LOAD_GIF,
    data,
    iter,
    time
})

export const fetchGifsFailure = error => ({
    type: FETCH_GIFS_FAILURE,
    payload: error
})

export const clearGif = () => ({
    type: CLEAR_GIF
})