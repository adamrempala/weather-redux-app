import styled from 'styled-components';

export const GifWrapper = styled.img`
    height: 192px;
`;
