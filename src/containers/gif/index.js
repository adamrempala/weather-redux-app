import React from 'react';
import { useSelector } from 'react-redux';

import { LOADING } from '../../weather-logic/const'
import { currentGifSelector, loadingGifSelector, isErrorGifSelector } from './selectors';
import Loader from 'react-loader-spinner'
import { GifWrapper } from './components'

export const Gif = () => {
    const addr = useSelector(currentGifSelector);
    const loading = useSelector(loadingGifSelector);
    const isError = useSelector(isErrorGifSelector);

    if (loading === LOADING.NOTHING) {
        return(
            <div></div>
        )
    } else if (loading === LOADING.TRUE) {
        return(
            <Loader
                type="TailSpin"
                color="#00BFFF"
                height={192}
                width={192}
                timeout={3000} //3 secs

            />
        )
    } else if (isError === true) {
        return(
            <GifWrapper
                src="/error.jpeg"
            />
        )
    } else {
        return(
            <GifWrapper
                src={addr}
            />
        )
    }
}