export const imgs = {
    svgs: {
        Rain: 'rain.svg',
        FEW_CLOUDS: 'few clouds',
        BROKEN_CLOUDS: 'broken clouds',
        SCATTERED_CLOUDS: 'scattered clouds',
        OVERCAST_CLOUDS: 'overcast clouds',
        CLEAR_SKY: 'clear sky',
        UNKNOWN: 'Unknown'
    }
}