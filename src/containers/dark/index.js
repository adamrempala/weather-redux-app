import { DarkWrapper } from './DarkWrapper'
import { modeSelector } from '../weather/selectors'
import { MODES } from '../../weather-logic/const'
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { changeMode } from '../weather/actions'

export const Dark = () => {
    const mode = useSelector(modeSelector);
    const dispatch = useDispatch();
    const onDarkClick = () => dispatch(changeMode())
    const message = mode === MODES.DARK ? 'Light mode' : 'Dark mode';

    return (
        <DarkWrapper
            mode={mode}
            onClick={ () => onDarkClick() }
        >
            {message}
        </DarkWrapper>
    )
}