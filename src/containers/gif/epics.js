import { ofType, combineEpics } from 'redux-observable'
import { map, mergeMap, delay, filter, catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax'

import { FETCH_GIFS_REQUEST, LOAD_GIF } from "./const";
import { timeSelector } from './selectors';
import { loadGif, fetchGifsFailure } from './actions';

// Gifs are loaded in a loop, but if watched prediction is changed, loop breaks.
const loadGifEpic = (action$, state$) =>
    action$.pipe(
        ofType(LOAD_GIF),
        delay(30000),
        filter(({time}) => time === timeSelector(state$.value)),
        map(({data, iter, time}) => loadGif(data, iter + 1, time))
    );

// After fetching request we try to fetch them. After succeeding we start a loading loop.
const fetchGifEpic = (action$, state$) =>
    action$.pipe(
        ofType(FETCH_GIFS_REQUEST),
        mergeMap(({time, key}) => {
            return ajax.getJSON(`https://api.tenor.com/v1/search?q="${key}"&key=I6GRU1NRAMPL&limit=8`)
            .pipe(
                map(response => {
                    const data = [];
                    response.results.forEach(element => {
                        data.push(element.media[0].gif.url);
                    });
                    return loadGif(data, 0, time);
                }),
                catchError(error => fetchGifsFailure(error.xhr.response))
            )
        })
        
    );

export const gifEpics = combineEpics(
    loadGifEpic,
    fetchGifEpic
)