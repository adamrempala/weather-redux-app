import React from 'react';
import { IconWrapper } from './IconWrapper'

// Used for select part of icon address
const selectSource = (type) => {
    if (type === 'Rain') return 'Rain';
    if (type === 'Clear') return 'Clear';
    if (type === 'Clouds') return 'Clouds';
    if (type === 'Extreme') return 'Extreme';
    if (type === 'Snow') return 'Snow';
    return 'Unknown'
}

export const Icon = ({type}) => {
    const source = `/svg/${selectSource(type)}.svg`;

    return(
        <IconWrapper
            type="image/svg+xml"
            data={source}
        >
            {type}
        </IconWrapper>
    )
}