import { fromJS } from 'immutable';
import {
    STARTING_RANGE,
    STARTING_MODE,
    STARTING_CACHE,
    LOADING,
    NO_WEATHER,
    NO_CITY,
    NO_ERROR
} from '../../weather-logic/const';

import {
    SET_RANGE,
    SET_MODE,
    SET_PREDICTION_SUCCESS,
    SET_PREDICTION_REQUEST,
    SET_PREDICTION_FAILURE,
    CLEAR_TABLE
} from './const';

import { getAnotherRange, getAnotherMode } from '../../weather-logic/logic'

export const WEATHER_REDUCER_NAME = 'Weather';

const initialState = fromJS({
    displayMode:{
        range: STARTING_RANGE,
        mode: STARTING_MODE,
    },    
    weatherData: {
        city: NO_CITY,
        prediction: {
            weather: NO_WEATHER,
            loading: LOADING.NOTHING,
            error: NO_ERROR
        },
        cache: STARTING_CACHE,
    }
    
});

export const weatherReducer = (state=initialState, action) => {
    switch(action.type) {
        case SET_RANGE:
            return state
                .updateIn(['displayMode', 'range'], getAnotherRange);
        case SET_MODE:
            return state
                .updateIn(['displayMode', 'mode'], getAnotherMode);
        case SET_PREDICTION_SUCCESS:
            const { city, payload, save } = action;
            if (save === false) {
                return state
                    .updateIn(['weatherData', 'city'],
                    () => city)
                    .updateIn(['weatherData', 'prediction'],
                    (prediction) => {
                        return prediction
                            .set('weather', payload)
                            .set('error', NO_ERROR)
                            .set('loading', LOADING.FALSE);
                    })
            } else {
                return state
                .updateIn(['weatherData', 'city'],
                    () => city)
                .updateIn(['weatherData', 'prediction'],
                (prediction) => {
                    return prediction
                        .set('weather', payload)
                        .set('error', NO_ERROR)
                        .set('loading', LOADING.FALSE);
                })
                .updateIn(['weatherData', 'cache'], (cache) => cache.push(payload))
            }
            
        case SET_PREDICTION_REQUEST:
            return state
                .updateIn(['weatherData', 'prediction'],
                (prediction) => {
                    return prediction
                        .set('loading', LOADING.TRUE)
                        .set('error', NO_ERROR);
                })
        case SET_PREDICTION_FAILURE:
            return state
                .updateIn(['weatherData', 'prediction'],
                (prediction) => {
                    return prediction
                        .set('weather', NO_WEATHER)
                        .set('error', action.payload)
                        .set('loading', LOADING.FALSE);
                })
        case CLEAR_TABLE:
            return state
                .updateIn(['weatherData', 'prediction'],
                (prediction) => {
                    return prediction
                        .set('loading', LOADING.NOTHING);
                })
        default:
            return state;
    }
}