import styled from 'styled-components';
import { theme } from 'styled-tools';
export const WeatherWrapper = styled.div`
    background: ${({mode}) => theme(`colors.mode.${mode}`)};
`;