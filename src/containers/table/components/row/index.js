import { RowWrapper } from'./RowWrapper'
import React from 'react';
import { RANGES } from '../../../../weather-logic/const'
import { Icon } from '../icon'
import { CellWrapper } from '../cell'

export const Row = ({data, range}) => {
    const date = new Date(data.dt * 1000).toString();
    const temp = data.temp;
    const pressure = data.pressure;
    const humidity = data.humidity;
    const windspeed = data.wind_speed;
    const weather = data.weather[0].main;

    if (range === RANGES.HOURLY) {
        return (
            <RowWrapper>
                <CellWrapper>
                    {date.slice(4, 21)}
                </CellWrapper>
                <CellWrapper>
                    {(temp - 273.15).toFixed(2)}
                </CellWrapper>
                <CellWrapper>
                    <Icon type={weather} />
                </CellWrapper>
                <CellWrapper>
                    {pressure}
                </CellWrapper>
                <CellWrapper>
                    {humidity}
                </CellWrapper>
                <CellWrapper>
                    {windspeed}
                </CellWrapper>
            </RowWrapper>
        )
    } else {
        return (
            <RowWrapper>
                <CellWrapper>
                    {date.slice(4, 15)}
                </CellWrapper>
                <CellWrapper>
                    {(temp.day - 273.15).toFixed(2)}
                </CellWrapper>
                <CellWrapper>
                    {(temp.night - 273.15).toFixed(2)}
                </CellWrapper>
                <CellWrapper>
                    <Icon type={weather} />
                </CellWrapper>
                <CellWrapper>
                    {pressure}
                </CellWrapper>
                <CellWrapper>
                    {humidity}
                </CellWrapper>
                <CellWrapper>
                    {windspeed}
                </CellWrapper>

            </RowWrapper>
        )
    }
}