import { combineEpics } from 'redux-observable';

import { weatherEpics } from './containers/weather/epics';
import { gifEpics } from './containers/gif/epics';

export const rootEpic = combineEpics(weatherEpics, gifEpics);
