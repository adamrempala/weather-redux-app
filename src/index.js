import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { compose, createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import createReducer from './reducer';
import { App } from './containers/app';
import { rootEpic } from './epics';

const DEBUG = true;

const reducer = createReducer();

let composeEnhancers, store;
const epicMiddleware = createEpicMiddleware();
const middleware = applyMiddleware(epicMiddleware)


if (DEBUG) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    store = createStore(reducer, undefined, composeEnhancers(middleware));
} else {
    composeEnhancers = compose;
    store = createStore(reducer, undefined, composeEnhancers(middleware));
}

epicMiddleware.run(rootEpic);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
