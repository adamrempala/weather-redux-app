export const fonts = {
    basic: {
        fontFamily: "'Century Gothic', Futura, sans-serif",
        fontSize: '14px'
    },
    button: {
        fontSize: '24px',
        fontWeight: 'bold',
        lineHeight: '34px',
        lineWidth: '190px'
    }
};
