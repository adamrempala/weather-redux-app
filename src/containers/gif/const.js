export const FETCH_GIFS_REQUEST = 'gif/FETCH_GIFS_REQUEST';
export const FETCH_GIFS_FAILURE = 'gif/FETCH_GIFS_FAILURE';
export const LOAD_GIF = 'gif/LOAD_GIF';
export const CLEAR_GIF = 'gif/CLEAR_GIF';