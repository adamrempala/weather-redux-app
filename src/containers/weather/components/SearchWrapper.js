import styled from 'styled-components';
import { theme } from 'styled-tools';

export const SearchWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: stretch;

    width: ${theme('dims.search.width')};
`;
