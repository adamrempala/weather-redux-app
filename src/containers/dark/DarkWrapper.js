import styled from 'styled-components';
import { theme, ifProp } from 'styled-tools';
import { getAnotherMode } from '../../weather-logic/logic'

export const DarkWrapper = styled.button`
    background: ${({ mode }) => theme(`colors.mode.${getAnotherMode(mode)}`)};
    color: ${({ mode }) => theme(`colors.mode.${mode}`)};
    border: 1px solid ${theme('colors.border')};
    float: left;

    font-size: ${theme('fonts.button.fontSize')};
    font-weight: ${theme('fonts.button.fontWeight')};
    line-height: ${theme('fonts.button.lineHeight')};

    height: ${theme('fonts.button.lineHeight')};
    width: ${theme('fonts.button.lineWidth')};

    margin-right: -1px;
    margin-top: -1px;
    padding: 0;
    text-align: center;

    cursor: ${ifProp('disabled', 'default', 'pointer')};

    outline: none;

    &:hover {
        background: ${ifProp(
            'disabled',
            theme('colors.background'),
            theme('colors.button.hoverBackground')
        )};
    }
`;
