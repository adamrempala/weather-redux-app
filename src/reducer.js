import { combineReducers } from 'redux';

import {WEATHER_REDUCER_NAME, weatherReducer} from './containers/weather/reducer'
import { gifReducer, GIF_REDUCER_NAME } from './containers/gif/reducer';


export default function createReducer() {
    return combineReducers({
        [WEATHER_REDUCER_NAME]: weatherReducer,
        [GIF_REDUCER_NAME]: gifReducer,
    });
}
