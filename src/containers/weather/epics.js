import { ofType, combineEpics } from 'redux-observable'
import { SET_PREDICTION_SUCCESS, SET_PREDICTION_FAILURE, SET_RANGE, SET_PREDICTION_REQUEST, ADD_TO_CACHE } from './const'
import { mostPopularHourlySelector, mostPopularDailySelector, rangeSelector, cacheSelector } from './selectors'
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax'
import { RANGES } from '../../weather-logic/const';
import { fetchGifsRequest } from '../gif/actions'
import { setPredictionSuccess, setPredictionFailure, addToCache } from './actions';
import { findCacheIndex } from '../../weather-logic/logic';

// First of all we check if expected location is in cache.
const setPredictionRequestEpic = (action$, state$) =>
    action$.pipe(
        ofType(SET_PREDICTION_REQUEST),
        map(({lon, lat, city}) => {
            let cache = cacheSelector(state$.value);
            let pos = findCacheIndex(cache, lon, lat);
        
            if (pos !== -1) {
                cache = cache._tail.array;
                return setPredictionSuccess(city, cache[pos], false);
            }

            return addToCache(lon, lat, city);
        })
    )

// If not, we add it asynchronously.
const addToCacheEpic = (action$, state$) => 
    action$.pipe(
        ofType(ADD_TO_CACHE),
        mergeMap(({lon, lat, city}) =>
        ajax.getJSON(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=current,minutely&appid=2a6a060cebd05ce37d50709557dca04a`)
            .pipe(
                map((response) => setPredictionSuccess(city, response, true)),
                catchError((error) => setPredictionFailure(error.xhr.response))
            )
        )
    )

// After watched forecast change we also change GIF set.
const updateGifOnMoveEpic = (action$, state$) => 
    action$.pipe(
        ofType(SET_PREDICTION_SUCCESS, SET_PREDICTION_FAILURE, SET_RANGE),
        map(() => ({
            key: rangeSelector(state$.value) === RANGES.DAILY
             ? mostPopularDailySelector(state$.value)
             : mostPopularHourlySelector(state$.value)
        })),
        map(({key}) => ({
            key: key === null ? 'lunch' : key
        })),
        map(({key}) => fetchGifsRequest(Number(new Date()), key))
    )

export const weatherEpics = combineEpics(
    updateGifOnMoveEpic,
    setPredictionRequestEpic,
    addToCacheEpic
);
