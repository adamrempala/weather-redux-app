import styled from 'styled-components';

export const MainRowWrapper = styled.tr`
    font-weight: 700;
    text-align: center;
`;