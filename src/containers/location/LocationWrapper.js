import styled from 'styled-components';
import { theme, ifProp } from 'styled-tools';

export const LocationWrapper = styled.button`
    background: ${theme(`colors.location`)};
    color: ${({ player }) => theme(`colors.players.${player}`)};
    border: 1px solid ${theme('colors.border')};

    float: left;

    font-size: ${theme('fonts.button.fontSize')};
    font-weight: ${theme('fonts.button.fontWeight')};
    line-height: ${theme('fonts.button.lineHeight')};

    height: ${theme('fonts.button.lineHeight')};
    width: ${theme('fonts.button.lineWidth')};

    margin-right: -1px;
    margin-top: -1px;
    padding: 0;
    text-align: center;

    cursor: pointer;

    outline: none;

    &:hover {
        background: ${ifProp(
            'disabled',
            theme('colors.background'),
            theme('colors.button.hoverBackground')
        )};
    }
`;