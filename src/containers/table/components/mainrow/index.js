import React from 'react'
import { MainRowWrapper } from './MainRowWrapper'
import { CellWrapper } from '../cell'
import { RANGES } from '../../../../weather-logic/const';

export const MainRow = ({range}) => {
    return (range === RANGES.DAILY)
    ? (
        <MainRowWrapper>
            <CellWrapper>
                {'Date'}
            </CellWrapper>
            <CellWrapper>
                {'Temp. day (°C)'}
            </CellWrapper>
            <CellWrapper>
                {'Temp. night (°C)'}
            </CellWrapper>
            <CellWrapper>
                {'Sky'}
            </CellWrapper>
            <CellWrapper>
                {'Pressure (hPa)'}
            </CellWrapper>
            <CellWrapper>
                {'Humidity (%)'}
            </CellWrapper>
            <CellWrapper>
                {'Windspeed (mps)'}
            </CellWrapper>
        </MainRowWrapper>
    )
    : (
        <MainRowWrapper>
            <CellWrapper>
                {'Date'}
            </CellWrapper>
            <CellWrapper>
                {'Temperature (°C)'}
            </CellWrapper>
            <CellWrapper>
                {'Sky'}
            </CellWrapper>
            <CellWrapper>
                {'Pressure (hPa)'}
            </CellWrapper>
            <CellWrapper>
                {'Humidity (%)'}
            </CellWrapper>
            <CellWrapper>
                {'Windspeed (mps)'}
            </CellWrapper>
        </MainRowWrapper>
    );
}