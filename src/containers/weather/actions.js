import { 
    SET_RANGE,
    SET_MODE,
    SET_PREDICTION_SUCCESS,
    SET_PREDICTION_REQUEST,
    SET_PREDICTION_FAILURE,
    CLEAR_TABLE,
    ADD_TO_CACHE
 } from './const';

export const changeRange = () => ({
    type: SET_RANGE
})

export const changeMode = () => ({
    type: SET_MODE,
})

export const setPredictionSuccess = (city, payload, save) => ({
    type: SET_PREDICTION_SUCCESS,
    city,
    payload,
    save
})

export const setPredictionRequest = (lon, lat, city) => ({
    type: SET_PREDICTION_REQUEST,
    lon,
    lat,
    city
})


export const setPredictionFailure = error => ({
    type: SET_PREDICTION_FAILURE,
    payload: error
})

export const clearTable = () => ({
    type: CLEAR_TABLE
})

export const addToCache = (lon, lat, city) => ({
    type: ADD_TO_CACHE,
    lon,
    lat,
    city
})