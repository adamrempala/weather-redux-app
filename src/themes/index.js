import { colors } from './colors';
import { fonts } from './fonts';
import { dims } from './dims';
import { imgs } from './imgs'

export const theme = {
    colors,
    fonts,
    dims,
    imgs
};
