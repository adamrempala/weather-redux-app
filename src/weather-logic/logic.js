import { RANGES, MODES, WEATHERTYPES} from './const';

export const getAnotherRange = (range) => {
    switch (range) {
        case RANGES.HOURLY:
            return RANGES.DAILY;
        case RANGES.DAILY:
            return RANGES.HOURLY;
        default:
            return RANGES.UNKNOWN;
    }
};

export const getAnotherMode = (mode) => {
    switch (mode) {
        case MODES.LIGHT:
            return MODES.DARK;
        case MODES.DARK:
            return MODES.LIGHT;
        default:
            return MODES.UNKNOWN;
    }
};

// Calculations are based on daily forecast only.
export const calculateNiceWeather = (data) => {
    if (data.length < 1)
        return WEATHERTYPES.UNKNOWN;
    let isNoRainy = 1;
    let avgTemp = {
        sum: 0,
        counted:0
    }
    let goodTemp = 1;

    for (const day of data) {
        const weathers = (day.weather);

        for (const w of weathers) {
            if (w.main === 'Rain') {
                isNoRainy = 0;
            }
        }
        const temp = day.temp;
        const min = temp.min;
        const max = temp.max;

        if (min - 273.15 < 15 || max - 273.15 > 30)
            goodTemp = 0;
        
        avgTemp.counted += 2;
        avgTemp.sum += min + max;
    }

    const average = avgTemp.sum / avgTemp.counted - 273.15;
    
    const result = (average >= 18 && average <= 25 ? 1 : 0) + isNoRainy + goodTemp;

    if (result === 3) return WEATHERTYPES.NICE;
    if (result > 1) return WEATHERTYPES.PASSABLE;
    else return WEATHERTYPES.NOT_NICE;
}

export const latitudeFormatter = (coord) => {
    if (coord < 0) {
        return `${Math.abs(coord).toFixed(2)}°S`;
    } else {
        return `${coord.toFixed(2)}°N`;
    }
}

export const longitudeFormatter = (coord) => {
    if (coord < 0) {
        return `${Math.abs(coord).toFixed(2)}°W`;
    } else {
        return `${coord.toFixed(2)}°E`;
    }
}

// Used for tenor API
export const mostPopularWeatherType = (object) => {
    if (object === null)
        return null;
    
    const all = [];
    object.forEach(element => {
        const weather = element.weather;
        weather.forEach(e => {
            all.push(e.description)
        })
    });

    all.sort();

    let max = 1;
    let now = 1;
    let last = all[0];
    let best = last;

    for (let i = 1; i < all.length; i++) {
        if (all[i] === last) {
            now++;

            if (now > max) {
                best = all[i];
                max = now;
            }
        } else {
            now = 1;
            last = all[i];
        }
    }

    return best;
}

// Returns position of element in c-array or -1 if element is not in it
export const findCacheIndex = (c, lon, lat) => {
    if (c.size !== 0) {
        const cache = c._tail.array
        for (let i = 0; i < cache.length; i++){
            if ((cache[i].lon).toFixed(2) === lon.toFixed(2) && cache[i].lat.toFixed(2) === lat.toFixed(2)){
                return i;
            }
        }
    }
    return -1;
}