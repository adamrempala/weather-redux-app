import styled from 'styled-components';
import { theme } from 'styled-tools';
import { getAnotherMode } from '../../../weather-logic/logic'

export const TableWrapper = styled.tbody`
    color: ${({ mode }) => theme(`colors.mode.${getAnotherMode(mode)}`)};
`