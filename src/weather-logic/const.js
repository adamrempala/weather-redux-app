export const STARTING_COORDS = {
    LON: 21,
    LAT: 52
};

export const RANGES = {
    HOURLY: 'hourly',
    DAILY: 'daily',
    UNKNOWN: 'unknown'
}

export const STARTING_RANGE = RANGES.HOURLY;

export const MODES = {
    LIGHT: 'light',
    DARK: 'dark',
    UNKNOWN: 'unknown'
}

export const LOADING = {
    NOTHING: 'NOTHING',
    TRUE: 'TRUE',
    FALSE: 'FALSE'
}

export const WEATHERTYPES = {
    NICE: 'nice',
    PASSABLE: 'passable',
    NOT_NICE: 'not nice',
    UNKNOWN: 'unknown'
}

export const WEATHERSKY = {
    LIGHT_RAIN: 'light rain',
    MODERATE_RAIN: 'moderate rain',
    HEAVY_INTENSITY_RAIN: 'heavy intensity rain',
    FEW_CLOUDS: 'few clouds',
    BROKEN_CLOUDS: 'broken clouds',
    SCATTERED_CLOUDS: 'scattered clouds',
    OVERCAST_CLOUDS: 'overcast clouds',
    CLEAR_SKY: 'clear sky',
    UNKNOWN: 'Unknown'
}

export const STARTING_MODE = MODES.LIGHT;

export const STARTING_CACHE = Array(0);

export const NO_WEATHER = null;

export const NO_ERROR = '';

export const NO_CITY = '';